from django.contrib import admin

from redirect.models import TargetUrl, Pattern


@admin.register(TargetUrl)
class TargetUrlAdmin(admin.ModelAdmin):
    list_display = ['target', 'active']
    list_editable = ['active']
    list_filter = ['active']
    search_fields = ['target']


@admin.register(Pattern)
class PatternAdmin(admin.ModelAdmin):
    list_display = ['slug', 'pattern']
