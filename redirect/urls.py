from django.urls import path
from django.views.generic import TemplateView

from redirect import views


app_name = 'redicrect'


urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('install/', views.PWAView.as_view(), name='install'),
    path('installed/', views.pwa_redirect_view, name='installed'),
    path('sw.js', (TemplateView.as_view(template_name="sw.js", content_type='application/javascript', )), name='sw.js'),
    path('manifest.json', (TemplateView.as_view(template_name="manifest.json", content_type='application/json', )), name='manifest'),
    path('<slug:pattern>/<str:uuid>/', views.redirect_view, name='redirect'),
]
