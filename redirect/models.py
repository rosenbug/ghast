from django.db import models


class TargetUrl(models.Model):
    active = models.BooleanField('Aktif', default=True)
    target = models.URLField('Hedef URL', max_length=200, blank=True)

    class Meta:
        verbose_name = 'Hedef URL'
        verbose_name_plural = 'Hedef URLler'

    def __str__(self):
        return self.target


class Pattern(models.Model):
    slug = models.SlugField('Kısayol', max_length=8)
    pattern = models.CharField('Kalıp', max_length=255)

    class Meta:
        verbose_name = 'Kalıp'
        verbose_name_plural = 'Kalıplar'

    def __str__(self):
        return self.slug
