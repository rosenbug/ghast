from django.http import HttpResponseRedirect
from django.views.generic import TemplateView

from .models import Pattern, TargetUrl


def redirect_view(request, pattern, uuid):
    domain_model = TargetUrl.objects.filter(active=True).last()
    domain = domain_model.target

    pattern_model = Pattern.objects.get(slug=pattern)
    pattern = pattern_model.pattern

    redirect_url = domain
    if redirect_url[-1] != '/':
        redirect_url += '/'
    redirect_url += pattern.replace('{{ uuid }}', uuid)
    return HttpResponseRedirect(redirect_url)


class IndexView(TemplateView):
    template_name = 'index.html'


class PWAView(TemplateView):
    template_name = 'install.html'


def pwa_redirect_view(request):
    domain_model = TargetUrl.objects.filter(active=True).last()
    redirect_url = domain_model.target
    return HttpResponseRedirect(redirect_url)
